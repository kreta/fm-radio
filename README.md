# FM Rado Transmitter

## Was tut es

UKW/FM Transmitter steuerbar über USB Serial und Python.  
Sende Audiosignal und RDS Daten (z.B. Sendername) wie ein "echter" Radiosender

## Hardware

* Rasperry Pi nano
* Adafruit Si4713 FM Transmitter Module
 
## Zusammenbau

### Hardware

![Hardware setup](setup.jpg)

### Software

Frisches Raspian-lite auf SDCard kopieren

```bash
pc $ sudo dd if=2018-04-18-raspbian-stretch-lite.img of=/dev/mmcblk0
```
Karte auswerfen und wieder einstecken, so dass Partitionen gemountet werden.  
Jetzt Änderungen durchführen, um sich später über USB einloggen zu können.

In `boot/config.txt` den USB Treiber aktivieren.
Folgendes am Ende einfügen

`dtoverlay=dwc2`

In `boot/cmdline.txt` den Treiber beim Booten laden.
Diese Kernel Parameter hinzufügen

`modules-load=dwc2,g_serial`

Und schließlich das Einloggen über USB erlauben

```bash
pc $ sudo ln -s /lib/systemd/system/getty@.service MOUNTPOINT_OF_ROOT_PARTITION/etc/systemd/system/getty.target.wants/getty@ttyGS0.service
```

Jetzt kann die SD Karte auswerfen, in den PI einstecken und den Pi über den `USB` USB port mit dem PC verbinden.
Nach einiger zeit sollte es möglich sein, sich zu verbinden und einzuloggen

```bash
minicom -D /dev/ttyACM0
```

Da Pakete installiert werden müssen zunächst mit einem lokalen WLAN verbinden

```bash
pi $ sudo bash
root $ wpa_passphrase "SSID" "Password" >> /etc/wpa_supplicant/wpa_supplicant.conf
root $ wpa_cli -i wlan0 reconfigure
root $ dlclient wlan0
root $ exit
```

Pakete installieren

```bash
pi $ sudo apt-get install git python-smbus
```

I2C Bus aktivieren unter `Interfacing Options`

```bash
pi $ sudo raspi-config
```

Code runterladen

```bash
pi $ git clone https://gitlab.com/kreta/fm-radio
```


## Nutzung

Nach freien Frequenzen suchen

```bash
pi $ ./scan.py -p
```

Sender starten

```bash
pi $ ./start.py --power 1.0 --name "My Station" 102.0
```



## Rechtliches

Der Betrieb von solchen Sendern ist rechtlich reglementiert und ohne Genehmigung nur bis maximal 50nW legal.  
Bitte stelle im Interesse deiner Mitmenschen sicher, dass die Reichweite deines Senders so begrenzt ist dass niemand dadurch gestört wird!

## Credits

Der Python Code zur Kommunikation mit dem FM Transmitter stammt von [daniel-j](https://github.com/daniel-j/Adafruit-Si4713-RPi).

#! /usr/bin/env python

from Adafruit_Si4713 import Adafruit_Si4713
import argparse
from math import floor
from sys import stdout

def print_graph(data):
	maxVal = max([x[1] for x in data])
	minVal = min([x[1] for x in data])-2
	width = 50
	plot_data = [(x[0], int(floor((x[1]-minVal)/float(maxVal-minVal)*width))) for x in data]
	for d in plot_data:
		print "{} | {}".format('#'*d[1]+' '*(width-d[1]), d[0])

def get_station_power(radio, start, end):
	power = []
	start_code = int(start*100)
	end_code = int(end*100)
	for f in range(start_code,end_code+10,10):
        	f_MHz = f/100.0
        	stdout.write("\rScanning: {}MHz".format(f_MHz))
        	stdout.flush()
        	radio.readTuneMeasure(f)
        	radio.readTuneStatus()
        	power.append((f_MHz, radio.currNoiseLevel))
	stdout.write("\n")
	stdout.flush()
	return power	

def get_best_frequency(power):
	value_only = [x[1] for x in power]
	return power[value_only.index(min(value_only))][0]

parser = argparse.ArgumentParser(description='Get best frequency to start a station')
parser.add_argument('--start', type=float, default = "87.5", help="Lower border of scanning range")
parser.add_argument('--end', type=float, default="108.0", help="Higher border of scanning range")
parser.add_argument('-p', help="Print bar chart", action='store_true')
args = parser.parse_args()

radio = Adafruit_Si4713()
if not radio.begin():
	raise Exception("Could not start the radio")

print("Scanning for stations in {} - {}MHz".format(args.start, args.end))

power_spectrum = get_station_power(radio, args.start, args.end)
if args.p:
	print_graph(power_spectrum)
print("Recommended frequency to start station: {}MHz".format(get_best_frequency(power_spectrum)))
	

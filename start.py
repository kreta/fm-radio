#! /usr/bin/env python

from Adafruit_Si4713 import Adafruit_Si4713
import argparse
import sys
from time import sleep
from sys import stdout, exit

parser = argparse.ArgumentParser(description='Start station at given frequency')
parser.add_argument('frequency', type=float, help="Station's frequency")
parser.add_argument('--power', type=float, default="1.0", help="Transmit power from 0 to 1")
parser.add_argument('-d', help="Keep running and return to shell", action='store_true')
parser.add_argument('--name', help="Nameto display via RDS")
args = parser.parse_args()

radio = Adafruit_Si4713()
if not radio.begin():
	raise Exception("Could not start the radio")

print("Starting station at {}MHz".format(args.frequency))

power_low = 88
power_high = 115
power = int(args.power*(power_high-power_low)+power_low)

frequency = int(args.frequency*100)

radio.readTuneMeasure(frequency)
radio.setTXpower(power)
if args.name and len(args.name) > 0:
        radio.setRDSstation(args.name)
        radio.beginRDS()

if not args.d:
	try:
		while True:
			sleep(0.1)
			status = radio.readASQ()
			stdout.write('\r\33[2KLevel: {0:3}dB'.format(status['inputlevel']))
			if status['overmodulation']:
				stdout.write(' \033[91m\033[1mClip\033[0m')
			stdout.flush()

	except KeyboardInterrupt:
		radio.setTXpower(0)
		stdout.write('\n')
		stdout.flush()
		exit(0)
